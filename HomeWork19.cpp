﻿
#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Im an Animal" "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meowwww!" "\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Wooof, Wooof!" "\n";
    }
};

class Horse : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Igogo!" "\n";
    }
};

int main()
{
    Animal* A[3]{ new Cat, new Dog, new Horse };

    for (int i = 0; i < 3; i++)
    {
        A[i]->Voice();
    }
}


